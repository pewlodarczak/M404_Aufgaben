interface IAnimal
{
    void MakeSound();
}

abstract class Animals : IAnimal
{
    public abstract void MakeSound();
    public virtual void Show()
    {
        Console.WriteLine("                           (o)(o)");
        Console.WriteLine("                          /      \\");
        Console.WriteLine("                         /       |");
        Console.WriteLine("                        /   \\  * |");
        Console.WriteLine("          ________     /    /\\__/");
        Console.WriteLine("  _      /        \\   /    /");
        Console.WriteLine(" / \\    /  ____    \\_/    /");
        Console.WriteLine("//\\ \\  /  /    \\         /");
        Console.WriteLine("V  \\ \\/  /      \\       /");
        Console.WriteLine("    \\___/        \\_____/");
        Console.WriteLine("\n");
    }
}

interface IFourLegAnimal
{
    public bool HasPaws { get; set; }
}

interface IBird
{
    public bool HasBeak { get; set; }
}
class Cat : Animals, IFourLegAnimal
{
    public override void MakeSound()
    {
        Console.WriteLine("Meow");
    }

    private bool _HasPaws = true;
    public bool HasPaws { get => _HasPaws; set => _HasPaws = value; }

    public override void Show()
    {   
        Console.WriteLine("\n");
        Console.WriteLine("/\\___/\\");
        Console.WriteLine(">^ , ^<");
        Console.WriteLine("\n");
    }
}

class Dog : Animals, IFourLegAnimal
{
    public override void MakeSound()
    {
        Console.WriteLine("Wuff");
    }

    public override void Show()
    {
        Console.WriteLine("\n");
        Console.WriteLine("  A.-.A");
        Console.WriteLine(" / , , \\");
        Console.WriteLine("=\\  t  /=");
        Console.WriteLine("\n");
    }

    private bool _HasPaws = true;
    public bool HasPaws { get => _HasPaws; set => _HasPaws = value; }
}

class Budgie : Animals, IBird
{
    public override void MakeSound()
    {
        Console.WriteLine("Tchirp");
    }

    private bool _HasBeak = true;
    public bool HasBeak { get => _HasBeak; set => _HasBeak = value; }

}

class AnimalStore
{
    List<IFourLegAnimal> fourLegAnim = new List<IFourLegAnimal>();
    List<IBird> birds = new List<IBird>();

    public void AddBird(IBird birdie)
    {
        birds.Add(birdie);
    }

    public void AddFourLeger(IFourLegAnimal forLeggie)
    {
        fourLegAnim.Add(forLeggie);
    }

    public void ListAllAnimals()
    {
        foreach(IFourLegAnimal fla in fourLegAnim)
        {
            Console.WriteLine(fla.GetType());
        }

        foreach(IBird brd in birds)
        {
            Console.WriteLine(brd.GetType());
        }

        foreach(Budgie brd in birds)
        {
            brd.MakeSound();
        }

        foreach(Animals anim in fourLegAnim)
        {
            anim.MakeSound();
        }

    }
}

class AnimalStoreGeneric<T> : List<Animals>
{
    static int MAX_ANIM = 100;
    T[] animStore = new T[MAX_ANIM];

    public void AddAnim(T anim)
    {
        for(int i = 0; i < MAX_ANIM; i++)
        {
            if(animStore[i] == null)
            {
                animStore[i] = anim;
                break;
            }
        }
    }

    public void ListAnim()
    {
        foreach(T a in animStore)
        {
            if(a == null)
                break;
            Console.WriteLine(a.GetType());
        }
    }

    public T GetAnim(int i)
    {
        return animStore[i];
    }

}