namespace ElectronicSuperStore;

interface ICustomer
{
    public bool Loggin(string username, string password);
}

class Customer : ICustomer
{
    protected string username = "", password = "";
    protected bool loggedin = false;
    protected string Name;

    public string CustomerName
    {
        get { return Name; }
        set { Name = value; }
    }

    public bool Loggin(string username, string password)
    {
        if(this.username.Equals(username) && this.password.Equals(password)) 
        {
            Console.WriteLine("Loggin successful, welcome user " + username);
            this.loggedin = true;
            return true;
        } else
        {
            Console.WriteLine("Wrong credentials");
            return false;
        }           
    }

}
class PrivateCustomer : Customer
{
    //private string Name, username = "alovelace", password = "totallysecret";
    //bool loggedin = false;
    //private string Name;
/*
    public string CustomerName
    {
        get { return Name; }
        set { Name = value; }
    }
*/
    public PrivateCustomer(string name)
    {
        this.Name = name;
    }

    public PrivateCustomer()
    {   
        username = "alovelace";
        password = "totallysecret";
    }
/*
    public bool Loggin(string username, string password)
    {
        if(this.username.Equals(username) && this.password.Equals(password)) 
        {
            Console.WriteLine("Loggin successful, welcome user " + username);
            this.loggedin = true;
            return true;
        } else
        {
            Console.WriteLine("Wrong credentials");
            return false;
        }           
    }*/
}

class BusinessCustomer : Customer
{
    //private string Name, username = "cbabbage", password = "totallysecret";
    //bool loggedin = false;
    //private string Name;
/*
    public string CustomerName
    {
        get { return Name; }
        set { Name = value; }
    }
*/
    public BusinessCustomer(string name)
    {
        this.Name = name;
    }

    public BusinessCustomer()
    {
        username = "cbabbage";
        password = "totallysecret";
    }
/*
    public bool Loggin(string username, string password)
    {
        if(this.username.Equals(username) && this.password.Equals(password)) 
        {
            Console.WriteLine("Loggin successful, welcome user " + username);
            this.loggedin = true;
            return true;
        } else
        {
            Console.WriteLine("Wrong credentials");
            return false;
        }
            
    }
*/
}