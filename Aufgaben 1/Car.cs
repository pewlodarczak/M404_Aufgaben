using System.Collections;

namespace Cars;

class Car
{
    private string CarType = "";
    private string FuelType = "";
    private float Gauge = 80.7F;
    private string Indicator = "";

    public string Type
    {
        get { return CarType; }
        set { CarType = value; }
    }

    public string Fuel
    {
        get { return FuelType; }
        set { FuelType = value; }
    }

    public Car(string Type)
    {
        this.CarType = Type;
    }

    public Car(string Type, string Fuel)
    {
        this.Type = Type;
        this.Fuel = Fuel;
    }


    public float NumericIndicator()
    {
        return Gauge;
    }

    public string StringIndicator()
    {
        if(Fuel.Equals("electricity"))
            return "battery status " + Gauge;
        else if(Fuel.Equals("petrol"))
            return "fill level " + Gauge;
        else
            return "unknown";
    }
}

class CarInventory  
{
    private static int MAX_SIZE = 10;
    private Car[] carInventory = new Car[MAX_SIZE];

    public string AddCar(Car car)
    {
        if(!(carInventory[carInventory.Length - 1] == null))
        {
            return "Inventory full";
        }

        for(int i = 0; i < carInventory.Length; i++)
        {
            if(carInventory[i] == null)
            {
                carInventory[i] = car;
                return "Car " + car.Type + " added";
            }
        }
        return "We should not get here";
    }

    public void ShowInventory()
    {
        foreach(Car c in carInventory)
        {
            if(c == null)
                return;
            Console.WriteLine("Element: " + c.Type);
        }
    }

    public void DeleteLastElement()
    {
        if(carInventory[carInventory.Length - 1] != null)
            carInventory[carInventory.Length - 1] = null;
        for(int i = 0; i < carInventory.Length - 2; i++)
        {
            if(carInventory[i + 1] == null)
            {
                carInventory[i] = null;
                return;
            }
        }
    }

    #region iterable
    int position = -1;
    public IEnumerator GetEnumerator()
    {
        return (IEnumerator)this;
    }

    public bool MoveNext()
    {
        position++;
        if(carInventory[position] == null)
            return false;
        return (position < carInventory.Length);
    }
    public void Reset()
    {
        position = -1;
    }

    public object Current
    {
        get { return carInventory[position]; }
    }
    #endregion   
}