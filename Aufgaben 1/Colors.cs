namespace Colors
{
    class WriteInColors
    {
        private string backGround, foreGround;

        public WriteInColors(string backGround, string foreGround)
        {
            this.backGround = backGround;
            this.foreGround = foreGround;
        }

        public void WriteString(string str)
        {
            // background color
            if(backGround.Equals("blue"))
            {
                Console.BackgroundColor = ConsoleColor.Blue;
            } else if(backGround.Equals("green"))
            {
                Console.BackgroundColor = ConsoleColor.Green;
            } else if(backGround.Equals("magenta"))
            {
                Console.BackgroundColor = ConsoleColor.Magenta;
            } else if(backGround.Equals("cyan"))
            {
                Console.BackgroundColor = ConsoleColor.Cyan;
            }
            // font color
            if(foreGround.Equals("white"))
            {
                Console.ForegroundColor = ConsoleColor.White;
            } else if(foreGround.Equals("green"))
            {
                Console.ForegroundColor = ConsoleColor.Green;
            } else if(foreGround.Equals("red"))
            {
                Console.ForegroundColor = ConsoleColor.Red;
            } else if(foreGround.Equals("black"))
            {
                Console.ForegroundColor = ConsoleColor.Black;
            }
            Console.WriteLine(str);
        }

        public static void Reset()
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
        }

        ~WriteInColors()
        {
            Console.ResetColor();
        }
    }
}