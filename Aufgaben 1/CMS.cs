namespace GroceriesCustomer
{
    class Customer
    {
        private string Name, OrderItem;

        public string CustomerName
        {
            get {return Name;}
            set {Name = value;}
        }

        public string CustomerOrder
        {
            get {return OrderItem;}
            set {OrderItem = value;}
        }

        public Customer(string name)
        {
            this.Name = name;
        }

        public void PlaceOrder(string item)
        {
            this.OrderItem = item;
            
        }

    }
}

namespace ElectronicsCustomer
{
    class Customer
    {
        private string Name;

        public string CustomerName
        {
            get {return Name;}
            set {Name = value;}
        }

        public Customer(string name)
        {
            this.Name = name;
        }

    }
}