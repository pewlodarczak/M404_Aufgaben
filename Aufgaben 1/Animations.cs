namespace Animations
{
    using System.Drawing;
    class ConsoleSpinner
    {
        private int counter = 0;
        private bool spinning = false;

        public ConsoleSpinner()
        {
            counter = 0;
        }

        public void StartSpinning()
        {
            spinning = true;
            Console.Write("\n\n");
            while (spinning)
            {
                spin();
            }
        }
        private void spin()
        {
            counter++;
            switch (counter % 4)
            {
                case 0:
                    Console.Write("/");
                    Thread.Sleep(100);
                    break;
                case 1:
                    Console.Write("-");
                    Thread.Sleep(100);
                    break;
                case 2:
                    Console.Write("\\");
                    Thread.Sleep(100);
                    break;
                case 3:
                    Console.Write("|");
                    Thread.Sleep(100);
                    break;
            }
            Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
        }

        public bool Enabled
        {
            get { return spinning; }
            set { spinning = value; }
        }
    }

    class ConsoleSequence
    {
        static string[,] sequence = null;
        public int Delay { get; set; } = 200;
        public int Sequence { get; set; } = 5;
        int totalSequences = 0;
        int counter;
        bool running = false;

        public ConsoleSequence()
        {
            counter = 0;
            sequence = new string[,] {
                { "/", "-", "\\", "|" },
                { ".", "o", "0", "o" },
                { "+", "x","+","x" },
                { "V", "<", "^", ">" },
                { ".   ", "..  ", "... ", "...." },
                { "=>   ", "==>  ", "===> ", "====>" },
                { "└   ", "┘  ", "┐ ", "┌" },
            };
            totalSequences = sequence.GetLength(0);
        }

        public void StartSequence()
        {
            running = true;
            Console.Write("\n\n");
            while (running)
            {
                Turn(displayMsg: "Working ", sequenceCode: Sequence);
            }
        }

        private void Turn(string displayMsg = "", int sequenceCode = 0)
        {
            counter++;
            Thread.Sleep(Delay);
            sequenceCode = sequenceCode > totalSequences - 1 ? 0 : sequenceCode;
            int counterValue = counter % 4;
            string fullMessage = displayMsg + sequence[sequenceCode, counterValue];
            int msglength = fullMessage.Length;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(fullMessage);
            Console.SetCursorPosition(Console.CursorLeft - msglength, Console.CursorTop);
        }
    }

    class MultiLineAnim
    {
        public static void MultiLineAnimation()
        {
            var counter = 0;
            for (int i = 0; i < 30; i++)
            {
                Console.Clear();

                switch (counter % 4)
                {
                    case 0:
                        {
                            Console.WriteLine("╔════╤╤╤╤════╗");
                            Console.WriteLine("║    │││ \\   ║");
                            Console.WriteLine("║    │││  O  ║");
                            Console.WriteLine("║    OOO     ║");
                            break;
                        };
                    case 1:
                        {
                            Console.WriteLine("╔════╤╤╤╤════╗");
                            Console.WriteLine("║    ││││    ║");
                            Console.WriteLine("║    ││││    ║");
                            Console.WriteLine("║    OOOO    ║");
                            break;
                        };
                    case 2:
                        {
                            Console.WriteLine("╔════╤╤╤╤════╗");
                            Console.WriteLine("║   / │││    ║");
                            Console.WriteLine("║  O  │││    ║");
                            Console.WriteLine("║     OOO    ║");
                            break;
                        };
                    case 3:
                        {
                            Console.WriteLine("╔════╤╤╤╤════╗");
                            Console.WriteLine("║    ││││    ║");
                            Console.WriteLine("║    ││││    ║");
                            Console.WriteLine("║    OOOO    ║");
                            break;
                        };
                }
                counter++;
                Thread.Sleep(200);
            }

        }

        public static void ColorfulAnimation()
        {
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 30; j++)
                {
                    Console.Clear();

                    // steam
                    Console.Write("       . . . . o o o o o o", Color.LightGray);
                    for (int s = 0; s < j / 2; s++)
                    {
                        Console.Write(" o", Color.LightGray);
                    }
                    Console.WriteLine();

                    var margin = "".PadLeft(j);
                    Console.WriteLine(margin + "                _____      o", Color.LightGray);
                    Console.WriteLine(margin + "       ____====  ]OO|_n_n__][.", Color.DeepSkyBlue);
                    Console.WriteLine(margin + "      [________]_|__|________)< ", Color.DeepSkyBlue);
                    Console.WriteLine(margin + "       oo    oo  'oo OOOO-| oo\\_", Color.Blue);
                    Console.WriteLine("   +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+", Color.Silver);

                    Thread.Sleep(200);
                }
            }
        }
    }
}