namespace Draw
{
    class DrawLines
    {
        private const string CLUBS = "♣";  
        public const string HEARTS = "♥";
        private const int LINE_LENGTH = 10;
        private const int SECONDS = 200; // 0.2 Seconds
        public void DrawHorizontalLine()

        {
            Console.Write("\n\n\t");
            for (int i = 0; i < LINE_LENGTH * 2; i++)
            {
                Console.Write(CLUBS);
                Thread.Sleep(SECONDS);
            }
            Console.Write("\n\n");
        }  

        public void DrawVerticalLine()
        {
            Console.Write("\n\n");
            for (int i = 0; i < LINE_LENGTH; i++)
            {
                Console.WriteLine("\t" + HEARTS);
                Thread.Sleep(SECONDS);
            }
            Console.Write("\n\n");
        }  

        public void DrawHorizontalLine(int numSymbols)
        {
            Console.Write("\n\n\t");
            for (int i = 0; i < numSymbols * 2; i++)
            {
                Console.Write(CLUBS);
                Thread.Sleep(SECONDS);
            }
            Console.Write("\n\n");
        }  

        public void DrawVerticalLine(int numSymbols)
        {
            Console.Write("\n\n");
            for (int i = 0; i < numSymbols; i++)
            {
                Console.WriteLine("\t" + HEARTS);
                Thread.Sleep(SECONDS);
            }
            Console.Write("\n\n");
        }  

    } // class DrawLines

    class DrawShape
    {
        private const string HASH = "#";
        private const string DIAMOND = "♦";
        private const string SPADES = "♠";
        private const string ARROW = "↔";
        public void DrawSquare(int length)
        {
            Console.Write("\n\n");
            for(int i = 0; i < length; i++) 
            {
                Console.Write("\t");
                for(int j = 0; j < length * 2; j++)
                {
                    Console.Write(DIAMOND);
                }
                Console.Write("\n");
            }
            Console.Write("\n\n");
        }

        public void DrawSquareLine(int length)
        {
            Console.Write("\n\n");
            for(int i = 0; i < length; i++) 
            {
                Console.Write("\t");
                if(i == 0 || i == length -1)
                {
                    for(int j = 0; j < length * 2; j++)
                    {
                        Console.Write(SPADES);
                    }
                } else {
                    for(int j = 0; j < length * 2; j ++)
                    {
                        if(j == 0 || j == (length * 2) - 1)
                        {
                            Console.Write(SPADES);
                        } else {
                            Console.Write(" ");
                        }
                    }
                }
                Console.Write("\n");
            }
            Console.Write("\n\n");
        }

        private int LINE_LENGTH = 10;
        private int LINE_HEIGHT = 10;
        private char MINUS = '-';
        public void DrawShapeOfSquare() // Aufgabe 3
        {
            int counter = 0;
            Console.WriteLine();
            //Console.WriteLine("-------------------------------------");
            for (int i = 0; i < LINE_HEIGHT; i++)
            {
                if (i == 0 || i == LINE_HEIGHT - 1)
                {
                    do
                    {
                        Console.Write(MINUS); // WriteLine fügt ein break rein, deswegen Write hier verwenden
                        counter++;
                    } while (counter < LINE_HEIGHT);
                    counter = 0; // Damit der COunter wieder auf Null gesetzt wird
                }
                else
                {
                    do
                    {
                        if (counter == 0 || counter == 9)
                        {
                            Console.Write(MINUS); // WriteLine fügt ein break rein, deswegen Write hier verwenden
                            counter++;
                        }
                        else
                        {
                            Console.Write(" ");
                            counter++;
                        }
                    } while (counter < LINE_HEIGHT);
                    counter = 0;
                }
                Console.WriteLine();

            }
        }


    } // class DrawShape
} // namespace