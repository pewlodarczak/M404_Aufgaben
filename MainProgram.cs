﻿using Draw;
using Colors;
using Animations;
using GroceriesCustomer;
using ElectronicsCustomer;
using ElectronicSuperStore;
using FileIO;
using Cars;
//using System.Diagnostics;

//[DebuggerDisplay($"{{{nameof(GetDebuggerDisplay)}(),nq}}")]
public class MainProgram
{
    public static void Main(string[] args)
    {
        /****** AUFGABEN 1 ******/
        /* Aufg 1
        DrawLines drawLines = new DrawLines();
        drawLines.DrawHorizontalLine();
        drawLines.DrawVerticalLine();
        /* Aufg 2
        drawLines.DrawHorizontalLine(5);
        drawLines.DrawVerticalLine(5);
        */
        /* Aufg 3
        DrawShape drawShape = new DrawShape();
        drawShape.DrawSquare(10);
        drawShape.DrawSquareLine(10);
        drawShape.DrawShapeOfSquare();
        */
        /* Aufg 4
        WriteInColors writeInColors1 = new WriteInColors("blue", "white");
        writeInColors1.WriteString("Tell me and I forget. Teach me and I remember. Involve me and I learn. - Benjamin Franklin");
        WriteInColors writeInColors2 = new WriteInColors("green", "red");
        writeInColors2.WriteString("It is during our darkest moments that we must focus to see the light. - Aristotle");
        WriteInColors writeInColors3 = new WriteInColors("cyan", "black");
        writeInColors3.WriteString("The purpose of our lives is to be happy. -Dalai Lama");
        WriteInColors.Reset();
        writeInColors2.WriteString("Only a life lived for others is a life worthwhile. -Albert Einstein");
        WriteInColors.Reset();
        */

        /* Aufg 5 
        var sportsCar = new Car("sports car");
        var transportCar = new Car("transport car");
        Console.WriteLine("Type: " + sportsCar.Type);
        Console.WriteLine("Type: " + transportCar.Type);
        /* Aufg 6 
        transportCar.Fuel = "petrol";
        sportsCar.Fuel = "electricity";
        Console.WriteLine("Type: " + sportsCar.Type + ", Fuel: " + sportsCar.Fuel);
        Console.WriteLine("Type: " + transportCar.Type + ", Fuel: " + transportCar.Fuel);
        Console.WriteLine(transportCar.Type + ": gauge " + sportsCar.NumericIndicator());

        Console.WriteLine(sportsCar.Type + ": gauge " + sportsCar.StringIndicator());
        Console.WriteLine(transportCar.Type + ": gauge " + transportCar.StringIndicator());

        var sportsCar2 = new Car("sports car", "petrol");
        var passengerCar = new Car("passenger car", "electricity");
        Console.WriteLine(sportsCar2.Type + ": gauge: " + sportsCar2.StringIndicator());
        Console.WriteLine(passengerCar.Type + ": gauge: " + passengerCar.StringIndicator());

        /* Aufg 7 
        var carInventory = new CarInventory();
        Console.WriteLine(carInventory.AddCar(sportsCar));
        Console.WriteLine(carInventory.AddCar(passengerCar));
        Console.WriteLine(carInventory.AddCar(sportsCar2));
        Console.WriteLine(carInventory.AddCar(transportCar));
        Console.WriteLine();
        carInventory.ShowInventory();
        Console.WriteLine();
        carInventory.DeleteLastElement();
        carInventory.ShowInventory();
        Console.WriteLine("\nforeach:");
        foreach(Car c in carInventory)
        {
            Console.WriteLine(c.Type + " " + c.Fuel);
        }

        /* Aufg 8
        GroceriesCustomer.Customer customer = new GroceriesCustomer.Customer("Jo Do");
        Console.WriteLine("Customer: " + customer.CustomerName);
        // change name
        customer.CustomerName = "Jo Matthew Do";
        Console.WriteLine("Customer: " + customer.CustomerName);
        // place order
        customer.PlaceOrder("Campbell's Tomato Soup");
        Console.WriteLine(customer.CustomerName + " ordered \"" + customer.CustomerOrder + "\"");
        // change order
        customer.CustomerOrder = "Mayonnaise";
        Console.WriteLine(customer.CustomerName + " changed order \"" + customer.CustomerOrder + "\"");
        
        /* Aufg 9
        ElectronicsCustomer.Customer electronicsCustomer = new ElectronicsCustomer.Customer("Jane Do");
        Console.WriteLine("Electronics customer: " + electronicsCustomer.CustomerName);
        */
        /* Aufg 10
        PrivateCustomer electricsPrivateCust = new PrivateCustomer("Ada Lovelace");
        //Console.WriteLine("Electronics customer: " + electricsPrivateCust.CustomerName);
        BusinessCustomer electronicsBusinessCust = new BusinessCustomer("Charles Babbage");
        //Console.WriteLine("Electronics customer: " + electronicsBusinessCust.CustomerName);
        if(electricsPrivateCust.GetType() == typeof(PrivateCustomer)) {
            Console.WriteLine("Electronics private customer: " + electricsPrivateCust.CustomerName);
        }
        if(electronicsBusinessCust.GetType() == typeof(BusinessCustomer)) {
            Console.WriteLine("Electronics business customer: " + electronicsBusinessCust.CustomerName);
        }
        */
        /* Aufg 11, 12, 13
        PrivateCustomer electricsPrivateCust = new PrivateCustomer();
        if(electricsPrivateCust.Loggin("alovelace", "totallysecret"))
        {
            electricsPrivateCust.CustomerName = "Ada Lovelace";
            Console.WriteLine("User " + electricsPrivateCust.CustomerName + " can order now");
        } else
            Console.WriteLine("Wrong credentials");

        BusinessCustomer businessCustomer = new BusinessCustomer();
        if(businessCustomer.Loggin("cbabbage", "totallysecret"))
        {
            businessCustomer.CustomerName = "Charles Babbage";
            Console.WriteLine("User " + electricsPrivateCust.CustomerName + " can order now");
        } else
            Console.WriteLine("Wrong credentials");
*/
        // Aufg 14
        //ConsoleSpinner consoleSpinner = new ConsoleSpinner();
        //consoleSpinner.StartSpinning();
        /*Aufg 15
        ConsoleSequence consoleSequence = new ConsoleSequence();
        consoleSequence.Delay = 300;
        consoleSequence.Sequence = 5;
        consoleSequence.StartSequence();
        */

        // Pendulum balls
        //MultiLineAnim.MultiLineAnimation();

        // Tschu-Tschu train
        //MultiLineAnim.ColorfulAnimation();

        /****** AUFGABEN 2 ******/
/*
        string AString = @"Life is a succession of lessons which must be lived 
        to be understood. -Ralph Waldo Emerson" + Environment.NewLine;
        string Two = "Hello";
        string Three = "World";
        string Path = ".\\Data\\";
        string FilePath1 = ".\\Data\\PlainText.txt";
        string FilePath2 = ".\\Data\\tmp\\PlainText.txt";

        TextFileIO textFileIO = new TextFileIO();
        textFileIO.WriteToFile(AString);

        textFileIO.AppendToFile(AString);
        textFileIO.FindStringInFile(FilePath1, AString);
        textFileIO.AppendToFile(Two);
        textFileIO.AppendToFile(Three);
        textFileIO.DeleteLine(FilePath1, Two);
        textFileIO.DeleteLineNumber(FilePath1, 3);

        textFileIO.MoveFile(FilePath1, FilePath2);
        textFileIO.ListDirectory(Path);

        string ErrorLogPath = ".\\Data\\ErrorLog.txt";
        string AuditLogPath = ".\\Data\\AuditLog.txt";
        var errorlog = new LogFileIO(ErrorLogPath);
        for(int i = 0; i < 100; i++)
        {
            errorlog.LogToFile("ERROR - Something went wrong");
        }
        errorlog.CloseLog();
        errorlog.ReadLog();

        var auditlog = new LogFileIO(AuditLogPath);
        for(int i = 0; i < 100; i++)
        {
            auditlog.LogToFile("User Miller loged in");
        }
        auditlog.CloseLog();
        auditlog.ReadLog();

        /*
                Console.WriteLine("File: " + textFileIO.WriteFromFile());
                Console.WriteLine("Enter a string:");
                string Line = Console.ReadLine();
                textFileIO.AppendToFile(Line);
                Console.WriteLine("File: " + textFileIO.WriteFromFile());
                */

        /****** AUFGABEN 3 ******/
        var aCat = new Cat();
        aCat.Show();
        aCat.MakeSound();
        var aDog = new Dog();
        aDog.Show();
        aDog.MakeSound();
        var aBudgie = new Budgie();
        Console.WriteLine();
        aBudgie.Show();
        aBudgie.MakeSound();
        
        Console.WriteLine("\nStore:");
        var aStore = new AnimalStore();
        aStore.AddBird(aBudgie);
        aStore.AddFourLeger(aCat);
        aStore.AddFourLeger(aDog);

        aStore.ListAllAnimals();
        Console.WriteLine("\nGeneric store");
        var aStoreGeneric = new AnimalStoreGeneric<IBird>();
        aStoreGeneric.AddAnim(aBudgie);
        aStoreGeneric.ListAnim();
        Console.WriteLine($"Has beak: {aStoreGeneric.GetAnim(0).HasBeak}");

        // Mit Vererbung von List
        Console.WriteLine("\nVererbung von List");
        var animStore = new AnimalStoreGeneric<Animals>();
        animStore.Add(aCat);
        animStore.Add(aDog);
        foreach(Animals anim in animStore)
        {
            anim.MakeSound();
            anim.Show();
        }

    }
}