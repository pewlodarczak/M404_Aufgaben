//using System.IO;

namespace FileIO;

class TextFileIO
{
    private string BFQuote = @"Tell me and I forget. Teach me and I remember. 
    Involve me and I learn. -Benjamin Franklin" + Environment.NewLine;

    string AString = @"Life is a succession of lessons which 
        must be lived to be understood. -Ralph Waldo Emerson" + Environment.NewLine;

    private string FilePath1 = ".\\Data\\PlainText.txt";
    //private string FilePath2 = ".\\Data\\PlainText2.txt";

    private string LogPath = ".\\Data\\LogFile.txt";

    public void WriteToFile(string AString)
    {
        File.WriteAllText(FilePath1, AString);
        return;
    }

    public void WriteToFile(string FilePath, string Line)
    {
        if (!File.Exists(FilePath))
        {
            using (StreamWriter sw = File.CreateText(FilePath))
            {
                sw.WriteLine(Line);
            }
        } else
        {
            AppendToFile(Line);
        }
    }

    public string WriteFromFile()
    {
        return File.ReadAllText(FilePath1);
    }

    public void AppendToFile(string AString)
    {
        using (StreamWriter sw = File.AppendText(FilePath1))
        {
            sw.WriteLine(AString);
        }
    }

    public void FindStringInFile(string FilePath, string AString)
    {
        if(File.ReadLines(FilePath).Any(line => line.Contains(AString)))
        {
            Console.WriteLine("String found");
        } else 
        {
            Console.WriteLine("String not found");
        }
    }

    public void DeleteLine(string FilePath, string Line)
    {
        File.WriteAllLines(FilePath, File.ReadLines(FilePath).Where(line => line != Line).ToList());
    }

    public void DeleteLineNumber(string FilePath, int number)
    {
        IEnumerable<string> lines = File.ReadLines(FilePath);
        //Console.WriteLine(String.Join(Environment.NewLine, lines));
        foreach(string line in lines)
        {
            Console.WriteLine(line);
        }    
    }

    public void MoveFile(string SourcePath, string DestinationPath)
    {
        if(File.Exists(DestinationPath))
        {
            File.Delete(DestinationPath);
        }
        File.Move(SourcePath, DestinationPath);
    }

    private FileStream f;
    private StreamWriter s;
    private String timeStamp;

    public void LogToFile(string LogEntry)
    {
        f = new FileStream(LogPath, FileMode.Append);
        s = new StreamWriter(f);
        timeStamp = DateTime.Now.ToString("yyyyMMddHHmmssffff");
        s.WriteLine(timeStamp + " " + LogEntry);
        s.Close();
    }

    public void CloseLog()
    {
        f.Close();
    }

    public void ReadLog()
    {
        int counter = 1;
        using (TextReader tr = File.OpenText(LogPath))
        {
            //string data = tr.ReadToEnd();
            //string Line = tr.ReadLine();

            //counter++;
            //tr.Close();
            string Line = "";
            while (true)
            {
                Line = tr.ReadLine();
                if (Line != null)
                {
                    Console.WriteLine("Line :" + counter + " " + Line);
                    counter++;
                }
            }
        }
        Console.ReadLine();
    }

public void ListDirectory(string Path)
    {
        string [] fileEntries = Directory.GetFiles(Path);
        foreach(string fileName in fileEntries)
            Console.WriteLine(fileName);
    }
}

class LogFileIO
{
    private FileStream f;
    private StreamWriter s;

    private String timeStamp;
    private string LogPath = "";

    public LogFileIO(string LogPath)
    {
        this.LogPath = LogPath;
    }
    public void LogToFile(string LogEntry)
    {
        f = new FileStream(LogPath, FileMode.Append);
        s = new StreamWriter(f);
        timeStamp = DateTime.Now.ToString("yyyyMMddHHmmssffff");
        s.WriteLine(timeStamp + " " + LogEntry);
        s.Close();
    }

    public void CloseLog()
    {
        f.Close();
    }

    public void ReadLog()
    {
        int counter = 1;
        using (TextReader tr = File.OpenText(LogPath))
        {
            string Line = "";
            while (true)
            {
                Line = tr.ReadLine();
                if (Line != null)
                {
                    Console.WriteLine("Line :" + counter + " " + Line);
                    counter++;
                }
            }
        }
        Console.ReadLine();
    }
}