//using System;
//using System.IO;

namespace Files;

public class FileIO
{
    //Field
    private string file1Path = ".\\Data\\file1.txt";
    //private string file2Path = ".\\files\\file2.txt";


    //Methods
    public void WriteToFile1(string input)
    {
        File.WriteAllText(file1Path, input);
        return;
    }

    public void AddToFile1(string input)
    {
        if (!File.Exists(file1Path))
        {   
            using (StreamWriter sw = File.CreateText(file1Path))
            {
                sw.WriteLine(input);
            }
        } else
        {
            using (StreamWriter sw = File.AppendText(file1Path))
            {
                sw.WriteLine(input);
            }
        }
    }
}